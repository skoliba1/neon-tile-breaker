//
//  Floor.swift
//  Project6
//
//  Created by Sharath Kolibailu Venkatesh on 4/12/17.
//  Copyright © 2017 skoliba1Andypancha1. All rights reserved.
//

import SpriteKit

//class for floor, ground , left and mwrite background
class FloorClass : SKSpriteNode
{
    
    func initializeFloor()
    {
        //name = "floor";
        physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.size.width, height: self.size.height));
        physicsBody?.friction = 0;
        physicsBody?.affectedByGravity = false;
        physicsBody?.categoryBitMask = ColliderType.BorderCategory;
        physicsBody?.collisionBitMask = ColliderType.BorderCategory;
        physicsBody?.contactTestBitMask = ColliderType.BallCategory;
    }
    
    
    
    
    
}
