//
//  GameScene.swift
//  Project6
//
//  Created by Sharath Kolibailu Venkatesh on 4/6/17.
//  Copyright © 2017 skoliba1Andypancha1. All rights reserved.
//

import SpriteKit
import GameplayKit

struct ColliderType
{
    //static let BallNode: UInt32 = 0;
    static let BallCategory: UInt32 = 1;
    static let BorderCategory: UInt32 = 2;
}

class GameScene: SKScene, SKPhysicsContactDelegate
{
    
    //let BallNode = SKSpriteNode(imageNamed:"neon_ball");
    //let BallCategory   : UInt32 = 0x1 << 0;
    //let BorderCategory   : UInt32 = 0x1 << 0
    
    private var mainCamera : SKCameraNode?;
    private var floor1 : FloorClass?;
    private var floor2 : FloorClass?;
    private var floor3 : FloorClass?;
    private var floor4 : FloorClass?;
    
    private var player : Player?;
    
    
    private func initializeGame()
    {
        
        
        //mainCamera = childNode(withName: "MainCamera") as? SKCameraNode!;
        
        
       
        
        physicsWorld.contactDelegate = self;
        //self.removeAllChildren();
        
        
        floor1 = childNode(withName: "floor1") as? FloorClass!;
        floor2 = childNode(withName: "floor2") as? FloorClass!;
        floor3 = childNode(withName: "floor3") as? FloorClass!;
        floor4 = childNode(withName: "floor4") as? FloorClass!;
        
        floor1?.initializeFloor();
        floor2?.initializeFloor();
        floor3?.initializeFloor();
        floor4?.initializeFloor();
        
        player = childNode(withName: "Ball") as? Player!;
        player?.initializePlayer();
        
        
        //physicsWorld.gravity = CGVector(dx:0 , dy:0);
        //BALL
//        BallNode.physicsBody = SKPhysicsBody(circleOfRadius:(BallNode.size.height));
//        BallNode.position = CGPoint(x:0, y:0);
//        BallNode.setScale(0.2);
//        BallNode.zPosition = 6;
//        BallNode.name = "Ball";
//        BallNode.physicsBody!.categoryBitMask = ColliderType.BallCategory;
//        BallNode.physicsBody!.contactTestBitMask = ColliderType.BorderCategory;
//        BallNode.physicsBody!.collisionBitMask = ColliderType.BorderCategory;
        //BallNode.physicsBody?.applyImpulse(CGVector(dx:5.0, dy:5.0));
        //self.addChild(BallNode);
        
        //edges of screen
        //var BorderNode = SKPhysicsBody();
        //    self.physicsBody!.categoryBitMask = BorderCategory;
        //    self.physicsBody!.contactTestBitMask = BallCategory;
        //    self.physicsBody!.collisionBitMask = BallCategory;
        //    self.physicsBody! = SKPhysicsBody(edgeLoopFrom: self.frame);
        //    self.physicsBody!.friction = 0;
    }
    
    override func didMove(to view: SKView)
    {
        physicsWorld.gravity = CGVector(dx:0.0, dy: 0.0);
        //self.player?.physicsBody!.applyImpulse(CGVector(dx: 10000, dy: -10000))
        initializeGame();
        self.player?.physicsBody!.applyImpulse(CGVector(dx: 200, dy: -200))
    }

    
    
    
func didBegin(_ contact: SKPhysicsContact)
{
    
    var first = SKPhysicsBody();
    var second = SKPhysicsBody();
    NSLog("here");
    if(contact.bodyA.node?.name == "Ball")
    {
        first = contact.bodyA;
        second = contact.bodyB;
    }
        
    else
    {
        first = contact.bodyB;
        second = contact.bodyA;
    }
    
    if(first.node?.name == "Ball" && (second.node?.name == "floor1" || second.node?.name == "floor2" || second.node?.name == "floor3" || second.node?.name == "floor4"))
    {
        
        NSLog("impulse");
        //physicsWorld.gravity = CGVector(dx:0.0, dy: 0.0);
        self.player?.physicsBody!.applyImpulse(CGVector(dx: 200, dy: -200));
        //first.applyImpulse(CGVector(dx:1000, dy:50.0))
        //self.player?.physicsBody?.velocity = CGVector(dx:0, dy:0);
        //self.player?.physicsBody?.applyImpulse(CGVector(dx: 1000,dy: 1000));
        
        
    }

}

func touchDown(atPoint pos : CGPoint) {
    
}

func touchMoved(toPoint pos : CGPoint) {
    
}

func touchUp(atPoint pos : CGPoint) {
    
}

override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

    
    
}

override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }

override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }

override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    
}


override func update(_ currentTime: TimeInterval)
{
    
}
    
}
