//
//  Player.swift
//  Project6
//
//  Created by Sharath Kolibailu Venkatesh on 4/6/17.
//  Copyright © 2017 skoliba1Andypancha1. All rights reserved.
//

import SpriteKit


class Player : SKSpriteNode
{
    
    
    func initializePlayer()
    {
        name = "Ball";
        physicsBody = SKPhysicsBody(circleOfRadius:(self.size.height));
        physicsBody?.affectedByGravity = true
        physicsBody?.allowsRotation = false
        physicsBody?.restitution = 1;
        physicsBody?.angularDamping = 0;
        physicsBody?.linearDamping = 0;
        physicsBody?.friction = 0;
        physicsBody?.categoryBitMask = ColliderType.BallCategory;
        physicsBody?.collisionBitMask = ColliderType.BorderCategory;
        physicsBody?.contactTestBitMask = ColliderType.BorderCategory;
        
    }

    
    
    
}
